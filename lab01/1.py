import math
from timeit import default_timer as timer


def f1(n):
    s = 0;
    for j in range(1, n):
        s = s + 1 / j
    return s


def f2(n):
    s = 0;
    for j in range(1, n):
        for k in range(1, n):
            s = s + k / j
    return s


def f3(n):
    s = 0;
    for j in range(1, n):
        for k in range(j, n):
            s = s + k / j
    return s


def f4(n):
    s = 0;
    for j in range(1, n):
        k = 2
        while k <= n:
            s = s + k / j
            k = k * 2
    return s


def f5(n):
    s = 0;
    k = 2
    while k <= n:
        s = s + 1 / k
        k = k * 2
    return s


nn = [2000, 4000, 8000, 16000, 32000]

#f1 n
# for n in nn:
#     start = timer()
#     f1(n)
#     stop = timer()
#     Tn = stop - start
#     Fn = n
#     print(n, Tn, Fn / Tn)

#f2 n*n
# for n in nn:
#     start = timer()
#     f2(n)
#     stop = timer()
#     Tn = stop - start
#     Fn = n*n
#     print(n, Tn, Fn / Tn)

#f3 n*n
# for n in nn:
#     start = timer()
#     f3(n)
#     stop = timer()
#     Tn = stop - start
#     Fn = n*n
#     print(n, Tn, Fn / Tn)

#f4 n*math.log(n,2)
# for n in nn:
#     start = timer()
#     f4(n)
#     stop = timer()
#     Tn = stop - start
#     Fn = n*math.log(n,2)
#     print(n, Tn, Fn / Tn)

#f5 math.log(n,2)
# for n in nn:
#     start = timer()
#     f5(n)
#     stop = timer()
#     Tn = stop - start
#     Fn = math.log(n,2)
#     print(n, Tn, Fn / Tn)


# inne funkcje czasu:

# Fn=math.log(n,2)
# Fn=n
# Fn=100*n
# Fn=n*math.log(n,2)
# Fn=n*n

'''
Pomiary dla f1 (n):
wartości  |    czas wykonania         |   iloraz
2000      |    9.490000002188026e-05  |   21074815.590504505
4000      |    0.00019959999985985633 |   20040080.174391236
8000      |    0.00039100000003600144 |   20460358.05438209
16000     |    0.0008442000000741245  |   18952854.77208615
32000     |    0.001518699999905948   |   21070652.53307548
'''
'''
Pomiary dla f2 (n*n):
2000 0.18563189999986207 21548020.57191125
4000 0.7374896000001172 21695221.193624232
8000 2.9563207000001057 21648530.891793206
16000 11.729888299999857 21824589.753340032
32000 46.898877099999936 21834211.463455305
'''

'''
Pomiary dla f3 (n*n):
2000 0.09635120000029929 41514791.72016098
4000 0.37879170000041995 42239573.887131795
8000 1.5310061999998652 41802574.019625545
16000 5.992944399999942 42716898.891970776
32000 23.767609199999697 43083845.38736076
'''

'''
Pomiary dla f4 (n*math.log(n,2)):
2000 0.0014231000000108907 15411122.597959623
4000 0.0031594000001859968 15149438.860489525
8000 0.006643800000347255 15612491.98830115
16000 0.013989999999921565 15972305.11478529
32000 0.029691100000036386 16129584.188817522
'''

'''
Pomiary dla f5 (math.log(n,2)):
2000 2.7000000955013093e-06 4061401.4432566413
4000 2.7000000955013093e-06 4431771.800526695
8000 9.200000022246968e-06 1409324.3753596623
16000 3.099999958067201e-06 4505091.765668772
32000 2.9000002541579306e-06 5160614.818293415
'''
