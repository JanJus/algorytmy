import random
from timeit import default_timer as timer


def function1(matrix):
    n = len(matrix)
    local_max = 0
    for x1 in range(n):
        for y1 in range(n):
            for x2 in range(x1, n):
                for y2 in range(y1, n):
                    submatrix = True
                    for y in range(y1, y2 + 1):
                        sub_row = matrix[y][x1: x2 + 1]
                        if 0 in sub_row:
                            submatrix = False
                            break
                    if submatrix is True and ((x2 - x1 + 1) * (y2 - y1 + 1) > local_max):
                        local_max = (x2 - x1 + 1) * (y2 - y1 + 1)
    return local_max


def function1_find(n):
    A = [[random.randint(0, 1) for _ in range(n)] for _ in range(n)]
    for row in A:
        print(row)
    print("The size of the biggest submatrix is: ", function1(A))


function1_find(6)


def time_count(nn):
    for n in nn:
        A = [[random.randint(0, 1) for _ in range(n)] for _ in range(n)]
        start = timer()
        function1(A)
        stop = timer()
        Tn = stop - start
        Fn = n ** 6
        print(n, Tn, Fn / Tn)


# time_count(nn=[10, 20, 30, 40, 50])


# wyniki pomiarów:
# 10 0.0022268050015554763 449073897.0414905
# 20 0.03581097499954922 1787161617.375836
# 30 0.12451078400044935 5854914542.963356
# 40 0.3160385410010349 12960444593.327581
# 50 0.8190903569993679 19076039494.885735


def function2(matrix):
    n = len(matrix)
    dp = [[0] * n for _ in range(n)]
    for i in range(n):
        for j in range(n):
            if matrix[i][j] == 0:
                dp[i][j] = 0
            else:
                dp[i][j] = dp[i][j - 1] + 1 if j > 0 else 1

    local_max = 0
    for j1 in range(n):
        for j2 in range(j1, n):
            width = j2 - j1 + 1
            for i1 in range(n):
                length = min(dp[i1][j1:j2 + 1])
                area = width * (length - 1)
                local_max = max(local_max, area)

    return local_max


def function_find_fun2(n):
    A = [[random.randint(0, 1) for _ in range(n)] for _ in range(n)]
    for row in A:
        print(row)
    print("The size of the biggest submatrix is: ", function1(A))


# function_find_fun2(6)


def time_count_fun2(nn):
    for n in nn:
        A = [[random.randint(0, 1) for _ in range(n)] for _ in range(n)]
        start = timer()
        function2(A)
        stop = timer()
        Tn = stop - start
        Fn = n ** 5
        print(n, Tn, Fn / Tn)


# time_count_fun2(nn=[10, 20, 30, 40, 50])

# wyniki:
# 10 0.0008191359993361402 122079850.08721861
# 20 0.003382321001481614 946095890.543284
# 30 0.013130731998899137 1850620361.6094882
# 40 0.041738660000191885 2453360984.74482
# 50 0.057137991998388316 5469215649.174627
