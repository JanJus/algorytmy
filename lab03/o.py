import sys
import random
from timeit import default_timer as timer

sys.setrecursionlimit(10**6)

def partition(A,p,r):
    x=A[r] # element wyznaczajacy podział
    i=p-1
    for j in range (p, r+1):
        if A[j]<=x :
            i=i+1
            A[i], A[j] = A[j], A[i]
    if i<r :
        return i
    else:
        return i-1
    
def quickSort(A,p,r):
    if p<r:
        q = partition(A,p,r)
        quickSort(A,p,q)
        quickSort(A,q+1,r)
    return A

nn = [1000, 5000, 10000, 15000]
print("Quicksort dla danych losowych (random.randint): ")
for n in nn:
    arr = []
    for i in range(n):
        arr.append(random.randint(0, n * 10))
    start = timer()
    quickSort(arr, 0, len(arr) - 1)
    stop = timer()
    Tn = stop - start
    print(n, Tn)
print()
print("Quicksort dla danych od najmniejszej do największej: ")
for n in nn:
    arr = []
    for i in range(n):
        arr.append(i)
    start = timer()
    quickSort(arr, 0, len(arr) - 1)
    stop = timer()
    Tn = stop - start
    print(n, Tn)