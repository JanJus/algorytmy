import sys
import random
from timeit import default_timer as timer

sys.setrecursionlimit(10**6)

def partition(A,p,r):
    x=A[r] # element wyznaczajacy podział
    i=p-1
    for j in range (p, r+1):
        if A[j]<=x :
            i=i+1
            A[i], A[j] = A[j], A[i]
    if i<r :
        return i
    else:
        return i-1
    
def quickSort(A,p,r):
    if p<r:
        q = partition(A,p,r)
        quickSort(A,p,q)
        quickSort(A,q+1,r)
    return A

nn = [1000, 5000, 10000, 15000]
print("Quicksort dla danych losowych (random.randint): ")
for n in nn:
    A = []
    for i in range(n):
        A.append(random.randint(0, n * 10))
    start = timer()
    quickSort(A, 0, len(A) - 1)
    stop = timer()
    Tn = stop - start
    print(n, Tn)
print()
print("Quicksort dla danych od najmniejszej do największej: ")
for n in nn:
    A = []
    for i in range(n):
        A.append(i)
    start = timer()
    quickSort(A, 0, len(A) - 1)
    stop = timer()
    Tn = stop - start
    print(n, Tn)

print()
print("Quicksort dla danych od największej do najmniejszej: ")
for n in nn:
    A = []
    for i in range(n, -1, -1):
        A.append(i)
    start = timer()
    quickSort(A, 0, len(A) - 1)
    stop = timer()
    Tn = stop - start
    print(n, Tn)


# wyniki:

# Quicksort dla danych losowych (random.randint): 
# 1000 0.007192984000539582
# 5000 0.03787426300004881
# 10000 0.09404039600030956
# 15000 0.14530974799981777

# Quicksort dla danych od najmniejszej do największej: 
# 1000 0.18035608700029115
# 5000 2.8467494619999343
# 10000 10.94051958
# 15000 25.508605344999523

# Quicksort dla danych od największej do najmniejszej: 
# 1000 0.12039617200025532
# 5000 1.8827710429995932
# 10000 7.311317863000113
# 15000 16.687202691000493