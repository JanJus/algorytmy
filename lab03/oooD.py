import random
import sys
from timeit import default_timer as timer
from threading import stack_size

stack_size(2 ** 16)
sys.setrecursionlimit(10 ** 6)


def partition(A, p, r):
    x = A[r]
    i = p
    for j in range(p, r):
        if A[j] < x:
            A[j], A[i] = A[i], A[j]
            i += 1
    A[i], A[r] = A[r], A[i]
    return i


def quickSort(A, p, r):
    if p < r:
        q = partition(A, p, r)
        quickSort(A, p, q - 1)
        quickSort(A, q + 1, r)


nn = [1000, 5000, 10000, 15000]
print("dane losowe: ")
for n in nn:
    arr = []
    for i in range(n):
        arr.append(random.randint(0, n * 10))
    start = timer()
    quickSort(arr, 0, len(arr) - 1)
    stop = timer()
    Tn = stop - start
    print(n, Tn)
print()
print("dane posortowane: ")
for n in nn:
    arr = []
    for i in range(n):
        arr.append(i)
    start = timer()
    quickSort(arr, 0, len(arr) - 1)
    stop = timer()
    Tn = stop - start
    print(n, Tn)