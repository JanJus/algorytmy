def heapify(A, heapSize, i):
    l = 2 * i + 1  # lewy syn
    r = 2 * i + 2  # prawy syn
    if l < heapSize and A[l] > A[i]:
        largest = l
    else:
        largest = i
    if r < heapSize and A[r] > A[largest]:
        largest = r
    if largest != i:
        A[i], A[largest] = A[largest], A[i]
        heapify(A, heapSize, largest)
    return A


def heapSort(A):
    n = len(A)

    for i in range(n // 2 - 1, -1, -1):
        heapify(A, n, i)

    for i in range(n - 1, 0, -1):
        A[i], A[0] = A[0], A[i]
        heapify(A, i, 0)

    return A

with open('input.txt', 'r') as f:
    num = list(map(int, f.read().split()))

finalAnswer = heapSort(num)

with open('output.txt', 'w') as f:
    f.write(' '.join(map(str, finalAnswer)))

    