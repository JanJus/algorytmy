class Node:
    def __init__(self, x):
        self.key = x
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None

    def wstaw(self, s):
        new_node = Node(s)
        new_node.next = self.head
        self.head = new_node

    def drukuj(self):
        current = self.head
        while current:
            print(current.key)
            current = current.next

    def szukaj(self, s):
        current = self.head
        while current:
            if current.key == s:
                return current
            current = current.next
        return None

    def usun(self, s):
        if self.head is None:
            return
        if self.head.key == s:
            self.head = self.head.next
            return
        current = self.head
        while current.next:
            if current.next.key == s:
                current.next = current.next.next
                return
            current = current.next

    def bezpowtorzen(self):
        unique_list = LinkedList()
        current = self.head
        unique_words = set()
        while current:
            if current.key not in unique_words:
                unique_list.wstaw(current.key)
                unique_words.add(current.key)
            current = current.next
        return unique_list

    def scal(self, other_list):
        if self.head is None:
            return other_list
        if other_list.head is None:
            return self

        result = LinkedList()
        current = self.head
        while current:
            result.wstaw(current.key)
            current = current.next

        current = other_list.head
        while current:
            result.wstaw(current.key)
            current = current.next

        return result


lista = LinkedList()

lista.wstaw("jabłko")
lista.wstaw("banan")
lista.wstaw("arbuz")
lista.wstaw("arbuz")
lista.wstaw("melon")

print("________________________________")
print("Wyświetlanie elementów: \n")
lista.drukuj()

print("________________________________")
print("Szukanie elementu: \n")
print(lista.szukaj("banan").key)

print("________________________________")
print("Usuwanie elementu: \n")
lista.usun("banan")
lista.drukuj()

print("________________________________")
print("Tworzenie kopii bez powtórzeń: \n")
unique_list = lista.bezpowtorzen()
unique_list.drukuj()

print("________________________________")
print("Scalanie list: \n")
lista2 = LinkedList()
lista2.wstaw("liczi")
lista2.wstaw("kiwi")
lista3 = lista.scal(lista2)
lista3.drukuj()
