class Node:
    def __init__(self, x):
        self.key = x
        self.next = None
        self.prev = None


class LinkedList:
    def __init__(self):
        self.head = None

    def listInsert(self, x):
        # wstawia wezel x do listy L
        # lista dwukierunkowa niecykliczna bez wartownika
        x.next = self.head
        if self.head != None:
            self.head.prev = x
        self.head = x
        x.prev = None

    def listPrint(self):
        x = self.head
        # uzupelnic

    def listSearch(self, k):
        # szuka wezla zawierajacego klucz k
        # lista dwukierunkowa niecykliczna bez wartownika
        x = self.head
        while x != None and x.key != k:
            x = x.next
        return x  # None oznacza, ze szukanego klucza
        # nie ma na liscie

    def listDelete(self, x):
        # usuwa wezel x z listy
        # lista dwukierunkowa niecykliczna bez wartownika
        if x.prev != None:
            x.prev.next = x.next
        else:
            self.head = x.next
        if x.next != None:
            x.next.prev = x.prev


l = LinkedList()
l.listInsert(Node(1))
l.listInsert(Node(2))
l.listPrint()
l.listDelete(l.listSearch(1))
l.listPrint()
