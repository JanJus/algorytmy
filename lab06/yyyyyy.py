class Nazwisko:
    def __init__(self, liczba, nazwisko):
        self.liczba = liczba
        self.nazwisko = nazwisko
        self.ilosc = 0

    def isEqual(self, other):
        if type(other) != type(self):
            return False
        if self.liczba == other.liczba and self.nazwisko == other.nazwisko:
            return True
        else:
            return False

    def getFromFile(path):
        file = open(path, 'r')
        inputList = file.readlines()
        file.close()
        result = []
        for elem in inputList:
            splitted = elem.split(' ')
            obj = Nazwisko(splitted[0], splitted[1].replace('\n', ''))
            result.append(obj)
        return result

    def prettyPrint(self):
        print(self.nazwisko, self.liczba, self.ilosc)

class Inserts:
    def __init__(self, m):
        self.m = m
        self.resultList = [None for _ in range(m)]
        self.licznikProb = 0

    def insertObj(self, obj, k, function):
        self.licznikProb += 1
        # print(type(obj))
        # obj.prettyPrint()
        hash = function(obj.nazwisko, k)
        if self.resultList[hash] == None:
            self.resultList[hash] = obj
        else:
            if obj.isEqual(self.resultList[hash]):
                self.resultList[hash].ilosc += 1
            else:
                self.insertObj(obj, k + 1, function)

    def insertElem(self, word, k, function):
        self.licznikProb += 1
        hash = function(word, k)
        if self.resultList[hash] == None:
            self.resultList[hash] = word
        else:
            self.insertElem(word, k + 1, function)

class OpenHash (Inserts):
    def __init__(self, m, hashType, procZapel):
        super().__init__(m)
        self.ileWstawic = int(m * procZapel)
        # self.inputList = self.getFromFile('3700.txt')
        self.inputList = Nazwisko.getFromFile('./nazwiskaASCII.txt')
        self.hashType = hashType
        self.hashFunc = self.__findHashFunc()

    def pojedynczyHash(self, word, k):
        letterList = list(word)
        resultNumber = 0
        for letter in letterList:
            resultNumber += ord(letter)
            resultNumber *= 111
        return ((resultNumber % self.m) + k) % self.m

    def innyPojedynczyHash(self, word, k):
        letterList = list(word)
        resultNumber = 0
        for letter in letterList:
            resultNumber += ord(letter)
            resultNumber *= 127
        return ((resultNumber % self.m) + k) % self.m

    def doubleHash(self, word, k):
        hash1 = self.pojedynczyHash(word, 0)
        hash2 = k * self.innyPojedynczyHash(word, 0)
        return (hash1 + hash2) % self.m

    def quarterHash(self, word, k):
        c = 2203
        return (self.pojedynczyHash(word, k) + c * k + c * k ** 2) % self.m

    def  insertAll(self):
        # inputlist = self.getFromFile('3700.txt')
        if len(self.inputList) >= self.m // 2:
            self.wordList = self.inputList[: self.ileWstawic]
        else:
            print("Nie mamy tyle słów aby wypełnić tablicę w zadanym stopniu\nUżyto maksymalnej posiadanej liczby")
        for word in self.wordList:
            # self.insertElem(word.replace('\n', ''), 0, self.hashFunc)
            self.insertObj(word, 0, self.hashFunc)
        # print(self.resultList)
        return self.resultList

    def __findHashFunc(self):
        match self.hashType.lower():
            case 's': return self.pojedynczyHash
            case 'd': return self.doubleHash
            case 'q': return self.quarterHash
            case _ :
                print("Nie istnieje taka funkcja haszująca\nW takim razie użyłem domyślnej")
                return self.pojedynczyHash

    def sredniaPodejsc(self):
        return self.licznikProb / self.ileWstawic

    def getFromFile(self, path):
        file = open(path, 'r')
        self.wordList = file.readlines()
        file.close()
        return self.wordList

class Tests:
    def __init__(self, m):
        self.m = m

    def __singleTest(self, hashType, procent):
        obj = OpenHash(self.m, hashType, procent)
        obj.insertAll()
        print(f'Dla {hashType} średnio jest średnio {obj.sredniaPodejsc()} prób')
        return obj.sredniaPodejsc()


    def mainTest(self):
        print(f"Testy przeprowadzono dla tablicy wielkosci {self.m}")
        print("Dla haszowania pojedyńczego")
        self.__singleTest('s', 0.5)
        self.__singleTest('s', 0.7)
        self.__singleTest('s', 0.9)

        print("\nDla haszowania podwójnego")
        self.__singleTest('d', 0.5)
        self.__singleTest('d', 0.7)
        self.__singleTest('d', 0.9)

        print("\nDla haszowania kwadratowego")
        self.__singleTest('q', 0.5)
        self.__singleTest('q', 0.7)
        self.__singleTest('q', 0.9)



#=test1 = OpenHash(2203, 's', 0.5)
# test1.insertAll()
# print(test1.sredniaPodejsc())
Tests(19997).mainTest()