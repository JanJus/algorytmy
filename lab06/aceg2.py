# wbudwana funkcja haszująca W
def W(key, m):
    return hash(key) % m


# dobre haszowanie D
def D(key, m):
    suma = 0
    for i in range(len(key)):
        suma = ((suma * 0xfe2ce6e0) ^ (ord(key[0]) * 0x4e0811a1)) ^ (
                    ((suma * 0x895cd7be) ^ (ord(key[i]) * 0xeb86d391)) ^ (
                        (ord(key[i]) * 0xe8c7b756) ^ (suma * 0xc040b340)))
    return abs(suma) % m


# słabe haszowanie S
def S(key, m):
    return ord(key[0]) % m


# wczytanie kluczy z pliku
def read_keys(filename):
    with open(filename, 'r') as f:
        keys = [line.strip() for line in f]
    return keys


keys = read_keys("/home/janju/studia/algorytmy/lab05/3700.txt")


# testy dla rozmiaru tablicy i typu haszowania
def test_hash(hash_func, m, keys):
    T = [[] for _ in range(m)]
    for key in keys:
        T[hash_func(key, m)].append(key)
    empty_lists = T.count([])
    nonempty_lists = [lst for lst in T if lst]
    max_len = max(len(lst) for lst in nonempty_lists)
    avg_len = sum(len(lst) for lst in nonempty_lists) / len(nonempty_lists)
    print(f"{hash_func.__name__} {m}")
    print(f"Ilość pustych list: {empty_lists}")
    print(f"Maksymalna długość listy: {max_len}")
    print(f"Średnia długość niepustych list: {avg_len:.2f}\n")


test_hash(W, 17, keys)
test_hash(D, 17, keys)
test_hash(S, 17, keys)
test_hash(W, 1031, keys)
test_hash(D, 1031, keys)
test_hash(S, 1031, keys)
test_hash(W, 1024, keys)
test_hash(D, 1024, keys)
test_hash(S, 1024, keys)

# W 17
# Ilość pustych list: 0
# Maksymalna długość listy: 245
# Średnia długość niepustych list: 220.24
#
# D 17
# Ilość pustych list: 0
# Maksymalna długość listy: 243
# Średnia długość niepustych list: 220.24
#
# S 17
# Ilość pustych list: 0
# Maksymalna długość listy: 543
# Średnia długość niepustych list: 220.24
#
# W 1031
# Ilość pustych list: 30
# Maksymalna długość listy: 12
# Średnia długość niepustych list: 3.74
#
# D 1031
# Ilość pustych list: 24
# Maksymalna długość listy: 13
# Średnia długość niepustych list: 3.72
#
# S 1031
# Ilość pustych list: 980
# Maksymalna długość listy: 343
# Średnia długość niepustych list: 73.41
#
# W 1024
# Ilość pustych list: 20
# Maksymalna długość listy: 11
# Średnia długość niepustych list: 3.73
#
# D 1024
# Ilość pustych list: 34
# Maksymalna długość listy: 11
# Średnia długość niepustych list: 3.78
#
# S 1024
# Ilość pustych list: 973
# Maksymalna długość listy: 343
# Średnia długość niepustych list: 73.41
#
#
# w większości przypadków rozmiar 1031 daje lepsze wyniki
#
# włąsan, dobrze napisana funkcja haszująca powinna być lepsza od słabej jak i tej wbudowanej ponieważ
# im mniej kolizji tym lepiej
