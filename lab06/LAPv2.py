DEL = "DEL"


class HashFunctions:
    def __init__(self, m):
        self.m = m

    def linearHash(self, word, k):
        letterList = list(word)
        resultNumber = 0
        for letter in letterList:
            resultNumber += ord(letter)
            resultNumber *= 111
        return ((resultNumber % self.m) + k) % self.m

    def otherLinearHash(self, word):
        letterList = list(word)
        resultNumber = 0
        for letter in letterList:
            resultNumber += ord(letter)
            resultNumber *= 127
        return resultNumber % self.m

    def doubleHash(self, word, k):
        hash1 = self.linearHash(word, 0)
        hash2 = k * self.otherLinearHash(word)
        return (hash1 + hash2) % self.m

    def squareHash(self, word, k):
        c = 2203
        return (self.linearHash(word, k) + c * k + c * k ** 2) % self.m


def HashInsertLH(T, k, hash_functions):
    i = 0
    myLength = len(T)
    total_attempts = 0
    while True:
        j = hash_functions.linearHash(k, i)
        if T[j] is None or T[j] == DEL:
            T[j] = k
            total_attempts += i + 1
            return j, total_attempts
        i += 1
        if i == myLength:
            raise ValueError("No space left...")


def HashSearchLH(T, k, hash_functions):
    i = 0
    while True:
        j = hash_functions.linearHash(k, i)
        if T[j] == k:
            return j
        i += 1
        if T[j] is None or i == m:
            return None


def calculate_average_attempts_LH(T, selectedKeys, hash_functions):
    total_attempts = 0
    for key in selectedKeys:
        _, attempts = HashInsertLH(T, key, hash_functions)
        total_attempts += attempts
    average_attempts = total_attempts / len(selectedKeys)
    return average_attempts
        

def run_tests_LH(m, keys, hash_functions):
    fill_levels = [0.5, 0.7, 0.9]
    for fill_level in fill_levels:
        T = [None] * m
        n = int(fill_level * m)
        selected_keys = keys[:n]
        average_attempts = calculate_average_attempts_LH(T, selected_keys, hash_functions)
        print("------")
        print(f"Fill Level for linearHash: {fill_level}")
        print(f"Average Attempts for linearHash: {average_attempts}")
        print("------")
    print("_____________________________________________________")


def HashInsertDH(T, k, hash_functions):
    i = 0
    myLength = len(T)
    total_attempts = 0
    while True:
        j = hash_functions.doubleHash(k, i)
        if T[j] is None or T[j] == DEL:
            T[j] = k
            total_attempts += i + 1
            return j, total_attempts
        i += 1
        if i == myLength:
            raise ValueError("No space left...")


def HashSearchDH(T, k, hash_functions):
    i = 0
    while True:
        j = hash_functions.doubleHash(k, i)
        if T[j] == k:
            return j
        i += 1
        if T[j] is None or i == m:
            return None
        

def calculate_average_attempts_DH(T, selectedKeys, hash_functions):
    total_attempts = 0
    for key in selectedKeys:
        _, attempts = HashInsertDH(T, key, hash_functions)
        total_attempts += attempts
    average_attempts = total_attempts / len(selectedKeys)
    return average_attempts


def run_tests_DH(m, keys, hash_functions):
    fill_levels = [0.5, 0.7, 0.9]
    for fill_level in fill_levels:
        T = [None] * m
        n = int(fill_level * m)
        selected_keys = keys[:n]
        average_attempts = calculate_average_attempts_DH(T, selected_keys, hash_functions)
        print("------")
        print(f"Fill Level for doubleHash: {fill_level}")
        print(f"Average Attempts for doubleHash: {average_attempts}")
        print("------")
    print("_____________________________________________________")


def HashInsertSH(T, k, hash_functions):
    i = 0
    myLength = len(T)
    total_attempts = 0
    while True:
        j = hash_functions.squareHash(k, i)
        if T[j] is None or T[j] == DEL:
            T[j] = k
            total_attempts += i + 1
            return j, total_attempts
        i += 1
        if i == myLength:
            raise ValueError("No space left...")


def HashSearchSH(T, k, hash_functions):
    i = 0
    while True:
        j = hash_functions.squareHash(k, i)
        if T[j] == k:
            return j
        i += 1
        if T[j] is None or i == m:
            return None
        

def calculate_average_attempts_SH(T, selectedKeys, hash_functions):
    total_attempts = 0
    for key in selectedKeys:
        _, attempts = HashInsertSH(T, key, hash_functions)
        total_attempts += attempts
    average_attempts = total_attempts / len(selectedKeys)
    return average_attempts


def run_tests_SH(m, keys, hash_functions):
    fill_levels = [0.5, 0.7, 0.9]
    for fill_level in fill_levels:
        T = [None] * m
        n = int(fill_level * m)
        selected_keys = keys[:n]
        average_attempts = calculate_average_attempts_SH(T, selected_keys, hash_functions)
        print("------")
        print(f"Fill Level for squareHash: {fill_level}")
        print(f"Average Attempts for squareHash: {average_attempts}")
        print("------")


with open("nazwiskaASCII.txt") as file:
    data = file.readlines()

keys = [line.split(" ", 1)[1].strip().replace("\n", "") for line in data]
m = 2143

hash_functions = HashFunctions(m)
run_tests_LH(m, keys, hash_functions)
run_tests_DH(m, keys, hash_functions)
run_tests_SH(m, keys, hash_functions)
