def lcsLength(x, y):
    m = len(x)
    n = len(y)
    c = [[0 for _ in range(n + 1)] for _ in range(m + 1)]
    b = [[0 for _ in range(n + 1)] for _ in range(m + 1)]

    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if x[i - 1] == y[j - 1]:
                c[i][j] = c[i - 1][j - 1] + 1
                b[i][j] = "\\"
            else:
                if c[i - 1][j] >= c[i][j - 1]:
                    c[i][j] = c[i - 1][j]
                    b[i][j] = "|"
                else:
                    c[i][j] = c[i][j - 1]
                    b[i][j] = "-"
    return c, b


def printTable(x, y, c, b):
    m = len(x)
    n = len(y)

    print("     ", end="")
    for j in range(n):
        print(f"{y[j]}  ", end="")
    print()

    for i in range(m + 1):
        if i > 0:
            print(x[i - 1], end=" ")
        else:
            print("  ", end="")

        for j in range(n + 1):
            print(f"{c[i][j]}{b[i][j]}", end=" ")
        print()


def getAllLCS(x, y, b, i, j, printed):
    if i == 0 or j == 0:
        return [""]

    if x[i - 1] == y[j - 1]:
        lcs_list = []
        lcs = getAllLCS(x, y, b, i - 1, j - 1, printed)
        for l in lcs:
            lcs_list.append(l + x[i - 1])
        return lcs_list
    else:
        lcs_list = []
        if c[i - 1][j] >= c[i][j - 1]:
            lcs_list.extend(getAllLCS(x, y, b, i - 1, j, printed))
        if c[i][j - 1] >= c[i - 1][j]:
            lcs_list.extend(getAllLCS(x, y, b, i, j - 1, printed))
        return lcs_list


x = input("sample1: ")
y = input("sample2: ")

c, b = lcsLength(x, y)
print("\nTable:")
printTable(x, y, c, b)

print("\nLongest Common Subsequences:")
printed = set()
lcs_list = getAllLCS(x, y, b, len(x), len(y), printed)
for lcs in lcs_list:
    if lcs not in printed:
        print(lcs)
        printed.add(lcs)
