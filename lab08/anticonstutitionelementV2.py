def lcsLength(x, y):
    m = len(x)
    n = len(y)
    c = [[0 for i in range(n + 1)] for j in range(m + 1)]
    b = [[0 for i in range(n + 1)] for j in range(m + 1)]

    for i in range(1, m + 1):
        for j in range(1, n + 1):
            if x[i - 1] == y[j - 1]:
                c[i][j] = c[i - 1][j - 1] + 1
                b[i][j] = "\\"
            else:
                if c[i - 1][j] >= c[i][j - 1]:
                    c[i][j] = c[i - 1][j]
                    b[i][j] = "|"
                else:
                    c[i][j] = c[i][j - 1]
                    b[i][j] = "-"
    return c, b


def printLCS(x, b, i, j):
    if i == 0 or j == 0:
        return ""
    if b[i][j] == "\\":
        printLCS(x, b, i - 1, j - 1)
        print(x[i - 1], end="")  # Wyświetlanie w poziomie z użyciem spacji jako separatora
    elif b[i][j] == "|":
        printLCS(x, b, i - 1, j)
    else:
        printLCS(x, b, i, j - 1)



x = input("sample1: ")
y = input("sample2: ")

c, b = lcsLength(x, y)
printLCS(x, b, len(x), len(y))
