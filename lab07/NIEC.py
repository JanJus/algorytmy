class Node:
    def __init__(self, x):
        self.key = x
        self.left = None  # lewy syn
        self.right = None  # prawy syn
        self.p = None  # ojciec


class BST:
    def __init__(self):
        self.root = None

    def BSTsearch(self, k):  # szuka wezla zawierajacego klucz k
        x = self.root
        while x is not None and x.key != k:
            if k < x.key:
                x = x.left
            else:
                x = x.right
        return x  # None oznacza, ze szukanego klucza
                        # nie ma w drzewie

    def BSTsearch_result(self, z):
        result = self.BSTsearch(z)
        if result is not None:
            print("Znaleziono węzeł o kluczu:", result.key)
        else:
            print("Węzeł o kluczu", z, "nie istnieje.")

    def BSTinsert(self, z):
        x = self.root
        y = None  # y jest ojcem x
        while x is not None:
            y = x
            if z.key < x.key:
                x = x.left
            else:
                x = x.right
        z.p = y
        if y is None:  # drzewo puste
            self.root = z
        else:
            if z.key < y.key:
                y.left = z
            else:
                y.right = z

    def BSTinOrder(self, x):
        # przechodzi i drukuje klucze poddrzewa
        # o korzeniu "x" w kolejnosci
        # "wewnętrznej (in order)"
        if x == None: return
        self.BSTinOrder(x.left)
        print(x.key)
        self.BSTinOrder(x.right)

    def BSTdelete(self, k):
        # Usuwa węzeł z drzewa
        z = self.BSTsearch(k)
        if z is None:
            print("Węzeł o kluczu", k, "nie istnieje.")
            return

        # Przypadek 1: Węzeł z nie ma synów
        if z.left is None and z.right is None:
            if z.p is None:
                self.root = None  # Drzewo było puste
            elif z == z.p.left:
                z.p.left = None
            else:
                z.p.right = None

        # Przypadek 2: Węzeł z ma tylko jednego syna
        elif z.left is None:
            if z.p is None:
                self.root = z.right
                z.right.p = None
            elif z == z.p.left:
                z.p.left = z.right
                z.right.p = z.p
            else:
                z.p.right = z.right
                z.right.p = z.p
        elif z.right is None:
            if z.p is None:
                self.root = z.left
                z.left.p = None
            elif z == z.p.left:
                z.p.left = z.left
                z.left.p = z.p
            else:
                z.p.right = z.left
                z.left.p = z.p

        # Przypadek 3: Węzeł z ma obu synów
        else:
            y = self.BSTminimum(z.right)  # Znajdowanie następnika z w prawym poddrzewie
            if y.p != z:
                self.BSTtransplant(y, y.right)
                y.right = z.right
                y.right.p = y
            self.BSTtransplant(z, y)
            y.left = z.left
            y.left.p = y

    def BSTminimum(self, x):
        # Znajduje węzeł o najmniejszym kluczu w poddrzewie o korzeniu x
        while x.left is not None:
            x = x.left
        return x

    def BSTtransplant(self, u, v):
        # Przenosi węzeł v z poddrzewa do miejsca węzła u
        if u.p is None:
            self.root = v
        elif u == u.p.left:
            u.p.left = v
        else:
            u.p.right = v
        if v is not None:
            v.p = u.p


bst = BST()
print("Insert:")
bst.BSTinsert(Node("kot"))
bst.BSTinsert(Node("pies"))
bst.BSTinsert(Node("antygona"))
bst.BSTinsert(Node("tak"))
bst.BSTinsert(Node("nie"))
print("Inserted! :D")
print("___________")
print("Print:")
bst.BSTinOrder(bst.root)
print("___________")
print("Search:")
bst.BSTsearch_result("aaaaa")
bst.BSTsearch_result("tak")
print("___________")
print("Delete:")
bst.BSTdelete("kot")
bst.BSTinOrder(bst.root)
