import networkx as nx
import matplotlib.pyplot as plt

class Node:
    def __init__(self, x):
        self.key = x
        self.left = None
        self.right = None
        self.p = None

class BST:
    def __init__(self):
        self.root = None

    def BSTsearch(self, k):
        x = self.root
        while x is not None and x.key != k:
            if k < x.key:
                x = x.left
            else:
                x = x.right
        return x

    def BSTsearch_result(self, z):
        result = self.BSTsearch(z)
        if result is not None:
            print("Znaleziono węzeł o kluczu:", result.key)
        else:
            print("Węzeł o kluczu", z, "nie istnieje.")

    def BSTinsert(self, z):
        x = self.root
        y = None
        while x is not None:
            y = x
            if z.key < x.key:
                x = x.left
            else:
                x = x.right
        z.p = y
        if y is None:
            self.root = z
        else:
            if z.key < y.key:
                y.left = z
            else:
                y.right = z

        if y is not None:
            G.add_edge(y.key, z.key)  # Add edge to the graph

    def BSTinOrder(self, x):
        if x == None: return
        self.BSTinOrder(x.left)
        print(x.key)
        self.BSTinOrder(x.right)

    def BSTdelete(self, k):
        z = self.BSTsearch(k)
        if z is None:
            print("Węzeł o kluczu", k, "nie istnieje.")
            return

        if z.left is None and z.right is None:
            if z.p is None:
                self.root = None
            elif z == z.p.left:
                z.p.left = None
            else:
                z.p.right = None
        elif z.left is None:
            if z.p is None:
                self.root = z.right
                z.right.p = None
            elif z == z.p.left:
                z.p.left = z.right
                z.right.p = z.p
            else:
                z.p.right = z.right
                z.right.p = z.p
        elif z.right is None:
            if z.p is None:
                self.root = z.left
                z.left.p = None
            elif z == z.p.left:
                z.p.left = z.left
                z.left.p = z.p
            else:
                z.p.right = z.left
                z.left.p = z.p
        else:
            y = self.BSTminimum(z.right)
            if y.p != z:
                self.BSTtransplant(y, y.right)
                y.right = z.right
                y.right.p = y
            self.BSTtransplant(z, y)
            y.left = z.left
            y.left.p = y

    def BSTminimum(self, x):
        while x.left is not None:
            x = x.left
        return x

    def BSTtransplant(self, u, v):
        if u.p is None:
            self.root = v
        elif u == u.p.left:
            u.p.left = v
        else:
            u.p.right = v
        if v is not None:
            v.p = u.p

    def BSTheight(self, x):
        if x is None:
            return 0
        else:
            left_height = self.BSTheight(x.left)
            right_height = self.BSTheight(x.right)
            return max(left_height, right_height) + 1


bst = BST()
G = nx.DiGraph()

print("Insert:")
bst.BSTinsert(Node("kot"))
bst.BSTinsert(Node("pies"))
bst.BSTinsert(Node("antygona"))

print("Inserted! :D")

print("___________")
print("Print (inorder):")
bst.BSTinOrder(bst.root)

pos = nx.spring_layout(G)
plt.figure(figsize=(10, 6))
nx.draw_networkx(G, pos, with_labels=True, node_size=500, font_size=10, node_color="lightgreen")
plt.title("My Search Tree")
plt.savefig("graf.png")
plt.show()

print("___________")
print("Search:")
bst.BSTsearch_result("chyba")
bst.BSTsearch_result("kot")
bst.BSTsearch_result("samolot")

print("___________")
print("Delete:")
bst.BSTdelete("tak")
bst.BSTdelete("antygona")
bst.BSTdelete("kot")
bst.BSTinOrder(bst.root)

print("___________")
print("Insertion! :D")
question = input("Choose the file you want the words to be taken from: ")

while True:
    if question == "3700":
        print("=====> 3700.txt")
        word_file = "3700.txt"
        break
    elif question == "words":
        print("=====> words.txt")
        word_file = "words.txt"
        break
    else:
        print("wrong file... :c")
        exit(0)

word_count = [500, 1000, 2000]

for count in word_count:
    bst_experiment = BST()

    with open(word_file, "r") as file:
        words = file.read().split()

    for word in words[:count]:
        if bst_experiment.BSTsearch(word) is None:
            bst_experiment.BSTinsert(Node(word))

    tree_height = bst_experiment.BSTheight(bst_experiment.root)
    print("Words inserted:", count)
    print("Height:", tree_height)
    print("___________")
